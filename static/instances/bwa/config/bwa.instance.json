{
  "applets": {
    "irs_monitor": {
      "map": {
        "chart_type": "map",
        "bin_by": "location.selection.id",
        "aggregation_names": [
          "number of rooms sprayed",
          "room spray coverage (%)"
        ],
        "property_layers": [
          {"property": "risk", "label": "Risk"},
          {"property": "Num_Rooms", "label": "Number of rooms"}
        ],
        "response_point_fields": [
          "recorded_on",
          "form_data.number_of_buildings_in_homesteads",
          "form_data.number_of_rooms",
          "_decorated.sprayed_status",
          "form_data.number_sprayed",
          "form_data.number_of_rooms_not_sprayed"
        ]
      },
      "table": {
        "chart_type": "table",
        "bin_by": "location.selection.id",
        "property_layers": [
          {
            "property": "__disarm_geo_name",
            "label": "Name"
          },
          {
            "property": "Num_Rooms",
            "label": "Number of rooms"
          }
        ],
        "aggregation_names": [
          "number of people in homestead (total)",
          "number of people in the homestead (<5 yrs)",
          "number of people in the homestead (>5 yrs)",
          "number of buildings visited",
          "number of rooms visited",
          "number of rooms sprayed",
          "room spray coverage (%)",
          "number of rooms sprayed with DDT",
          "number of rooms sprayed with lambda-cyhalothrin",
          "number of other structures visited",
          "Total other structures sprayed",
          "number of rooms not sprayed",
          "number of rooms not sprayable (N)",
          "number of rooms not sprayable (%)",
          "Number of sprayable rooms not sprayed",
          // Refusal reasons
          "number of rooms not sprayed - locked",
          "number of rooms not sprayed - locked (%)",
          "number of rooms not sprayed - nobody",
          "number of rooms not sprayed - nobody (%)",
          "number of rooms not sprayed - refusal",
          "number of rooms not sprayed - refusal (%)",
          "number of rooms not sprayed - baby",
          "number of rooms not sprayed - baby (%)",
          "number of rooms not sprayed - patient",
          "number of rooms not sprayed - patient (%)",
          "number of rooms not sprayed - funeral",
          "number of rooms not sprayed - funeral (%)",
          "number of rooms not sprayed - kitchen",
          "number of rooms not sprayed - kitchen (%)",
          "number of rooms not sprayed - food storage",
          "number of rooms not sprayed - food storage (%)"
        ]
      },
      "charts": [
        {
          "id": "variable_definition",
          "chart_type": "text",
          "style": {
            "height_constraint": "none",
            "width_constraint": "half"
          },
          "options": {
            "title": "Variable definitions",
            "text": "**Room**: \nAn enclosed space (e.g. bedrooms, living rooms, kitchen)\n\n**Other structure**: \nBesides rooms, any other structures that are sprayable (e.g. bathrooms, toilets, passages, open-kitchens)\n\n**Building**: \nOne or more conjoined rooms\n\n**Homestead**: \nA yard with one or more buildings\n\n**Household**: \nA group of people who share the same home\n\n**Head of household**: \nAny person responsible for the people who live in a household"
          }
        },
        {
          "id": "room_coverage",
          "style": {
            "height_constraint": "none",
            "width_constraint": "half"
          },
          "options": {
            "layout": {
              "showlegend": true,
              "title": "Room coverage as % of target",
              "yaxis": {
                "title": "% coverage"
              },
              "xaxis": {
                "title": "Period commencing"
              }
            },
            "chart_type": "line",
            "cumulative": true,
            "time_series": true,
            "bin_by": "recorded_on",
            "geographic_level_refactor_this_key_name": "location.selection.id",
            "multi_series": [{
              "aggregation_name": "room spray coverage (%)"
            }]
          }
        },
        {
          "id": "spray_status_absolute",
          "style": {
            "height_constraint": "none",
            "width_constraint": "half"
          },
          "options": {
            "layout": {
              "showlegend": true,
              "title": "Spray status",
              "yaxis": {
                "title": "# of households"
              },
              "xaxis": {
                "title": "Spray status"
              }
            },
            "chart_type": "bar",
            "bin_by": "_decorated.sprayed_status",
            "single_series": {
              "aggregation_name": "count"
            }
          }
        },
        {
          "id": "spray_status_pie",
          "style": {
            "height_constraint": "none",
            "width_constraint": "half"
          },
          "options": {
            "layout": {
              "title": "Sprayed status proportion"
            },
            "chart_type": "pie",
            "generate_series_from": "_decorated.sprayed_status"
          }
        },
        {
          "id": "room_coverage_by_week",
          "style": {
            "height_constraint": "none",
            "width_constraint": "half"
          },
          "options": {
            "chart_type": "bar",
            "time_series": true,
            "cumulative": true,
            "bin_by": "recorded_on",
            "layout": {
              "showlegend": true,
              "title": "Spray room coverage",
              "yaxis": {
                "title": "# of rooms"
              },
              "xaxis": {
                "title": "Period commencing"
              },
              "barmode": "stack"
            },
            "multi_series": [
              {
                "aggregation_name": "number of rooms sprayed",
                "colour": "green"
              },
              {
                "aggregation_name": "number of rooms not sprayed",
                "colour": "red"
              }
            ]
          }
        },
        {
          "id": "total_rooms_sprayed_per_week",
          "style": {
            "height_constraint": "none",
            "width_constraint": "half"
          },
          "options": {
            "layout": {
              "showlegend": true,
              "title": "Total number of rooms sprayed per week",
              "yaxis": {
                "title": "# of rooms"
              },
              "xaxis": {
                "title": "Period commencing"
              }
            },
            "chart_type": "bar",
            "time_series": true,
            "bin_by": "recorded_on",
            "multi_series": [
              {
                "aggregation_name": "number of rooms sprayed"
              }
            ]
          }
        },
        {
          "id": "spray_status_pie_reason",
          "style": {
            "height_constraint": "none",
            "width_constraint": "half"
          },
          "options": {
            "layout": {
              "title": "Refusal reasons for not spraying room"
            },
            "chart_type": "pie",
            "multi_series": [
              {
                "aggregation_name": "number of rooms not sprayed - locked"
              },
              {
                "aggregation_name": "number of rooms not sprayed - nobody"
              },
              {
                "aggregation_name": "number of rooms not sprayed - refusal"
              },
              {
                "aggregation_name": "number of rooms not sprayed - baby"
              },
              {
                "aggregation_name": "number of rooms not sprayed - patient"
              },
              {
                "aggregation_name": "number of rooms not sprayed - funeral"
              },
              {
                "aggregation_name": "number of rooms not sprayed - kitchen"
              },
              {
                "aggregation_name": "number of rooms not sprayed - food storage"
              }
            ]
          }
        }
      ],
      "title": "Dashboard"
    },
    "irs_plan": {
      "title": "Planning + Management",
      "table_output": [
        {
          "display_name": "District name",
          "source_field": "name_2"
        },
        {
          "display_name": "Village name",
          "source_field": "VILLAGE"
        },
        {
          "display_name": "Number of buildings enumerated",
          "source_field": "NumStrct"
        },
        {
          "display_name": "Estimated number of rooms",
          "source_field": "Num_Rooms"
        },
        {
          "display_name": "Predicted risk",
          "source_field": "risk"
        }
      ]
    },
    "irs_record_point": {
      "title": "Data Collection + Reporting",
      "metadata": {
        "show": true,
        "optional_fields": []
      }
    },
    "meta": {},
    "debug": {}
  },
  "map_focus": {
    "centre": {
      "lat": -20.37552680342694,
      "lng": 24.158935546875004
    },
    "zoom": 7
  },
  "instance": {
    "title": "Botswana IRS Database",
    "location_name": "Botswana",
    "slug": "bwa"
  },
  "spatial_hierarchy": {
    "data_version": 10,
    "markers": {
      "planning_level_name": "villages",
      "record_location_selection_level_name": "villages",
      "denominator_fields": {
        "estimated_rooms": "Num_Rooms"
      }
    },
    "levels": [
      {
        "field_name": "ID_2",
        "display_field_name": "NAME_2",
        "name": "districts"
      },
      {
        "group_by_field": "name_2",
        "field_name": "Id", // __disarm_geo_id
        "display_field_name": "VILLAGE", // __disarm_geo_name
        "name": "villages"
      },
      {
        "field_name": "ClusterID",
        "display_field_name": "SP_ID_1",
        "name": "clusters"
      }
    ]
  }
}

